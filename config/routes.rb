Rails.application.routes.draw do
  # Sign Up requests
  get '/sign-up', to: 'registration#new', as: :registration_new
  post '/sign-up', to: 'registration#create', as: :registration_create

  # Sign In Requests
  get '/sign-in', to: 'sessions#new', as: :sessions_new
  post '/sign-in', to: 'sessions#create', as: :sessions_create
  get '/sign-out', to: 'sessions#destroy', as: :sessions_logout

  # Videos Requests
  get '/videos/edit/:id', to: 'videos#edit', as: :edit_video
  get '/videos/player/:slug', to: 'videos#player', as: :player_video
  delete '/videos/delete/:id', to: 'videos#destroy', as: :delete_video
  put '/videos/update/:id', to: 'videos#update', as: :update_video
  post '/video/:id/view/add', to: 'videos#add_view', as: :add_view_video

  resource :videos
  root 'sessions#new'
end
