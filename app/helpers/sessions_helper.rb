module SessionsHelper
  def sign_in(user)
    session[:user_id] = user.id
  end

  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    else
      @current_user = nil
    end
  end

  def block_access
    if current_user.present?
      redirect_to :videos
    end
  end

  def logged_in?
    !current_user.nil?
  end

  def redirect_if_logged_in
    unless logged_in?
      flash[:alert] = "Voce precisa estar logado";
      redirect_to :sessions_new
    end
  end

  def logout
    session.delete(:user_id)
    @current_user = nil
  end
end
