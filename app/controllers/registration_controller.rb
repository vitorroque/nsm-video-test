class RegistrationController < ApplicationController
  before_action :block_access, only: [:new]
  def new
    @user = User.new
    render "registration/new"
  end

  def create
    user = User.create(user_params)
    if user.save!
      redirect_to :sessions_new, notice: 'Usuario criado com sucesso'
    else
      redirect_to :sessions_new, alert: user.errors.full_messages
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :name, :password);
  end
end
