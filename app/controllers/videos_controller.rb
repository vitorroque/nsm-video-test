class VideosController < ApplicationController
  before_action :redirect_if_logged_in, except: [:create, :update, :destroy]

  layout "home"

  def show
    @other_videos = Videos::FindOthers.call(current_user) 
  end

  def create
    @video = Videos::Create.call(current_user, video_params)

    if @video.persisted?
      redirect(:videos, { notice: I18n.t(:video_created) })
    else
      redirect(:videos, { alert: @video.errors.full_messages })
    end
  end

  def edit
    @video = Videos::Find.call(current_user, params[:id])
  end

  def update
    video = Videos::Update.call(current_user, params[:id], video_params)
    if video.save
      redirect(:videos, {notice: I18n.t(:video_updated)})
    else
      redirect(:videos, {alert: video.errors.full_messages})
    end
  end

  def destroy
    if Videos::Destroy.call(current_user, params[:id])
      redirect(:videos, {notice: I18n.t(:video_deleted)})
    else
      redirect(:videos, {alert: I18n.t(:video_not_deleted)})
    end
  end

  def player
    @video = Videos::Show.call(params[:slug])
    render "videos/player"
  end

  def add_view
    unless VideoViews::Add.call(params[:id], current_user)
      render :json => {
        :error => I18n.t(:multiple_same_views)
      }, status: 400
    end
  end

  private

  def redirect(destination, flash_hash)
    redirect_to destination, flash_hash 
  end

  def video_params
    params.require(:video).permit(:name, :url)
  end
end
