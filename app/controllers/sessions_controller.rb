class SessionsController < ApplicationController
  before_action :block_access, only: [:new]

  def create
    @user = User.find_by(email: user_params[:email])
    if @user && @user.authenticate(user_params[:password])
      sign_in @user
      redirect_to :videos
    else
      redirect_to :sessions_new, alert: "Usuario e/ou senha invalidos"
    end
  end

  def destroy
    logout
    redirect_to :sessions_new, notice: "Deslogado com sucesso"
  end

  private
  def user_params
    params.require(:user)
      .permit(:email, :password)
  end
end
