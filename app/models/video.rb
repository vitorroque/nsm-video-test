class Video < ApplicationRecord
  validates :name, presence: true
  validates :url, presence: true, format: /.m3u8/i

  belongs_to :user
  has_many :video_views

  def get_views
    self.video_views.count
  end

  def is_same_viewer?(user)
    self.video_views
      .select { |view| 
        puts view.inspect
        view.user_id == user.id
      }
      .any?
  end
end
