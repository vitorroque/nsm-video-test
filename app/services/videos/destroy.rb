module Videos
  class Destroy < ApplicationService
    def initialize(user, id)
      @user = user
      @id = id
    end

    def call
      video = @user.videos.find(@id)
      video.destroy
    end
  end
end
