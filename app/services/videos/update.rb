module Videos

  # Update an video
  class Update < ApplicationService
    def initialize(user, video_id, params)
      @user = user
      @video_id = video_id
      @params = params
    end

    def call
      video = @user.videos.find(@video_id)
      @params[:slug] = @params[:name].parameterize
      video.update(@params)
      video
    end
  end
end
