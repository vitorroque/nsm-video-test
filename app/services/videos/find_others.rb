module Videos

  # Find other videos which is not mine
  class FindOthers < ApplicationService
    def initialize(user)
      @user = user
    end

    def call
      Video.where.not(user_id: @user.id)
    end
  end
end
