module Videos
  class Create < ApplicationService
    def initialize(user, params)
      @user = user
      @params = params
    end

    def call
      @params[:slug] = @params[:name].parameterize
      @user.videos.create(@params)
    end
  end
end
