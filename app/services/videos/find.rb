module Videos
  class Find < ApplicationService
    def initialize(user, id)
      @user = user
      @id = id
    end
    def call
      @user.videos.find(@id)
    end
  end
end
