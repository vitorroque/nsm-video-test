module Videos
  class Show < ApplicationService
    def initialize(slug)
      @slug = slug
    end
    def call
      Video.find_by(slug: @slug)
    end
  end
end
