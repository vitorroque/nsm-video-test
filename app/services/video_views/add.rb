module VideoViews
  class Add < ApplicationService
    def initialize(video_id, user)
      @video_id = video_id
      @user = user
    end

    def call
      video = Video.find(@video_id)

      unless video.is_same_viewer? @user
        view = VideoView.create!(video_id: @video_id, user_id: @user.id)
        view.persisted? 
      end
    end
  end
end
