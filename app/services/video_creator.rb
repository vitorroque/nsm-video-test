module Services
  class VideoCreator < ApplicationService
    def initialize(video)
      @video = video
    end

    def call
      video = Video.create(@video)
       if video.persisted?
         # do the right things
       end
    end
  end
end
