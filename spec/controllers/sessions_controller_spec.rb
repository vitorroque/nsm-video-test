require 'rails_helper'

describe SessionsController, type: :controller do
  before(:each) do
    @user = User.create(name: 'John Doe', email: 'john@doe.com', password: 'senha123')
  end

  it 'should return session id' do
    post :create, params: {
      user: {
        email: 'john@doe.com',
        password: 'senha123'
      }
    }
    expect(session[:user_id]).not_to be_nil
  end
end
