require 'rails_helper'

describe VideosController, type: :controller do

  let(:video_params) do
    {
      video: {
        name: 'video test',
        url: 'http://localhost/video.m3u8',
      }
    }
  end

  before(:each) do
    @user = User.create(name: 'John Doe', email: 'john@doe.com', password: 'senha123')
    session[:user_id] = @user.id
  end


  it 'should create a video' do
    post :create, params: video_params
    expect(Video.last).not_to be_nil
    expect(Video.last.slug).to eq('video test'.parameterize)
    expect(self).to redirect_to :videos
  end

  it 'should create a video with a wrong extension' do
    post :create, params: {
      video: {
        name: 'video test',
        url: 'http://localhost/video.mp4'
      }
    } 

    for alert in flash[:alert] do
      expect(alert).not_to be_nil
    end
  end

  it 'should update a video' do
    post :create, params: video_params
    put :update, params: {
      id: Video.last.id,
      video: {
        name: 'video 2'
      }
    }

    expect(Video.last).not_to be_nil
    expect(Video.last.name).to eq('video 2')
    expect(Video.last.slug).to eq('video 2'.parameterize)
  end

  it 'should delete a video' do
    post :create, params: video_params
    delete :destroy, params: { id: Video.last.id }

    expect(Video.last).to be_nil
  end

  it 'should add view to a video' do
    post :create, params: video_params
    post :add_view, params: { id: Video.last.id }

    expect(Video.last.get_views).to eq(1)
  end

  it 'should not add view if the same user play the video again' do
    post :create, params: video_params
    add_view_twice

    expect(Video.last.get_views).to eq(1)
  end

  private
  def add_view_twice
    post :add_view, params: { id: Video.last.id }
    post :add_view, params: { id: Video.last.id }
  end
end
