require 'rails_helper'

describe RegistrationController, type: :controller do
  let(:user_params) do
    {
      user: {
        name: 'John Doe',
        email: 'john@doe.com',
        password: 'senha123',
      }
    }
  end

  it 'should create an user' do
    post :create, params: user_params
    expect(User.last).not_to be_nil
  end
end
