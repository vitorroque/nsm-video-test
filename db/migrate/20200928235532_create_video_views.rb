class CreateVideoViews < ActiveRecord::Migration[6.0]
  def change
    create_table :video_views do |t|
      t.integer :user_id, null: false
      t.integer :video_id, null: false

      t.index :user_id
      t.index :video_id

      t.timestamps
    end
  end
end
