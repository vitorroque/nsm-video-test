class RemoveViewsFromVideoTable < ActiveRecord::Migration[6.0]
  def change
    remove_column :videos, :views
  end
end
