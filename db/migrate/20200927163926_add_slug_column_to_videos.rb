class AddSlugColumnToVideos < ActiveRecord::Migration[6.0]
  def change
    add_column :videos, :slug, :string, default: ''
  end
end
